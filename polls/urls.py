from django.urls import path
from . import views
from django.contrib.auth.views import auth_login
from polls.views import VehicleList

urlpatterns = [
    path('', views.index, name='index'),
    path('reviews/', views.reviews, name='reviews'),
    path('about/', views.about, name='about'),
    path('login/', auth_login, {'template name': 'polls/login.html'}),
    path('vehicles/', VehicleList.as_view(), {'template name': '/polls/vehicle_list.html'}),
]
