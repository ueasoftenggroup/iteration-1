from django.shortcuts import render
from django.views.generic import ListView
from polls.models import Vehicle

# Create your views here.

# from polls.models import Vehicle, Booking, User, Branch, PaymentInformation


def index(request):
    context = {}

    return render(request, 'polls/index.html', context=context)


def reviews(request):
    context = {}

    return render(request, 'polls/reviews.html', context=context)


def about(request):
    context = {}

    return render(request, 'polls/about.html', context=context)


def login(request):
    context = {}

    return render(request, 'polls/login.html', context=context)


class VehicleList(ListView):
    model = Vehicle


# def vehicles(request):
#     context = {}
#
#     return render(request, 'polls/vehicle_list.html', context=context)
