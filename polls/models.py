from django.db import models


class Customer(models.Model):
    customer_id = models.IntegerField(primary_key=True)
    customer_name = models.CharField(max_length=50)
    customer_email = models.CharField(max_length=50)


class Vehicle(models.Model):
    vehicle_id = models.IntegerField(primary_key=True)
    type = models.CharField(max_length=50)
    brand = models.CharField(max_length=50)
    reg_plate = models.CharField(max_length=50)
    colour = models.CharField(max_length=50)
    baggage = models.IntegerField(default=1)


class Booking(models.Model):
    booking_id = models.IntegerField(primary_key=True)
    status = models.CharField(max_length=50)
    booking_date = models.DateField
    customer_id = models.ForeignKey(Customer, on_delete=models.DO_NOTHING,)
    vehicle_id = models.ForeignKey(Vehicle, on_delete=models.DO_NOTHING,)

# Create your models here.
