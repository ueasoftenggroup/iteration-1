from django.contrib import admin

from .models import Booking
from .models import Vehicle
from .models import Customer

admin.site.register(Vehicle)
admin.site.register(Customer)
admin.site.register(Booking)

# Register your models here.

